# SC21 Tutorial

## Agenda

* 8:00 Intro and Welcome (10 mins)
* 8:10 Kubernetes Intro and Architecture - Slides (18 + 7 mins)
* 8:35 Hands On: Basic Kubernetes (40 mins)
* 9:15 Kubernetes Scheduling - Slides (19+ 6 mins)
* 9:40 Hands On: Scheduling - Part 1 (20 mins)
* 10:00 Break (30 mins)
* 10:30 Hands On: Scheduling - Part 1 (20 mins)
* 10:50 Persistent Storage - Slides (22 + 8 mins)
* 11:20 Hands On: Storage (40 mins)
* 12:00 Lunch break (60 mins)
* 01:00 Building you own applications - Slides (12 + 8 mins)
* 01:20 Hands On: Apps (60 mins)
* 02:20 Monitoring - Slides(13 + 7 mins)
* 02:40 Hands On: Monitoring - Part 1 (20 mins)
* 03:00 Break (30 mins)
* 03:30 Hands On: Monitoring - Part 2 (20 mins)
* 03:50 Federation - Slides (15 + 5 mins)
* 04:10 Hands On: Federation (40 mins)
* 4:50 Closeout and questions
# SC21 Tutorial

Basic Kubernetes\
Hands on session

## Setup

There are several basic step you must take to get access to the cluster.

1. Install the kubectl Kubernetes client.\
   Instructions at <https://kubernetes.io/docs/tasks/tools/install-kubectl/>\
   If you have homebrew installed on mac, use that. Otherwise try downloading the static binary (the curl way) 

2. Get your configuration file for the PRP/Nautilus Kubernetes cluster from organizers, and put it as \~/.kube/config.

The config files for this tutorial will have the right namespace pre-set. In general you need to be aware of which namespace you’re working in, and either set it with `kubectl config set-context nautilus --namespace=the_namespace` or specify in each `kubectl` command by adding `-n namespace`.

## Explore the system

To get the list of cluster nodes (although you may not have access to all of them), type:

```
kubectl get nodes
```

Right now you don't have anything running in the namespace, and these commands will return `No resources found in sc21 namespace.`, but later these will be useful to see what's running:

List all the pods in your namespace

```
kubectl get pods
```

List all the deployments in your namespace

```
kubectl get deployments
```

List all the services in your namespace

```
kubectl get services
```

## Launch a simple pod

Let’s create a simple generic pod, and login into it.

You can copy-and-paste the lines below, but please do replace “username” with your own id.\
All the participants in this hands-on session share the same namespace, so you will get name collisions if you don’t.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-<username>
spec:
  containers:
  - name: mypod
    image: centos:centos8
    resources:
      limits:
        memory: 100Mi
        cpu: 100m
      requests:
        memory: 100Mi
        cpu: 100m
    command: ["sh", "-c", "sleep infinity"]
```

Reminder, indentation is important in YAML, just like in Python.

*If you don't want to create the file and are using mac or linux, you can create yaml's dynamically like this:*

```bash
kubectl create -f - << EOF
<contents you want to deploy>
EOF
```

Now let’s start the pod:

```
kubectl create -f pod1.yaml
```

See if you can find it:

```
kubectl get pods
```

Note: You may see the pods from the other participants, too.

If it is not yet in Running state, you can check what is going on with

```
kubectl get events --sort-by=.metadata.creationTimestamp
```

Then let’s log into it

```
kubectl exec -it pod-<username> -- /bin/bash
```

You are now inside the (container in the) pod!

Does it feel any different than a regular, dedicated node?

Try to create some directories and some files with content.

(Hello world will do, but feel free to be creative)

**❗️ Note that some containers don't have /bin/bash installed. In other examples if you see the "Not Found" error, you might try `/bin/sh`. Some containers (not in this tutorial) don't have any shell installed, and you can't exec into those.**

We will want to check the status of the networking.

But ifconfig is not available in the image we are using; so let’s install it

```
yum install net-tools
```

Now check the the networking:

```
ifconfig -a
```

Get out of the Pod (with either Control-D or exit).

You should see the same IP displayed with kubectl

```
kubectl get pod -o wide pod-<username>
```

We can now destroy the pod

```
kubectl delete -f pod1.yaml
```

Check that it is actually gone:

```
kubectl get pods
```

Now, let’s create it again:

```
kubectl create -f pod1.yaml
```

Does it have the same IP?

```
kubectl get pod -o wide pod-<username>
```

Log back into the pod:

```
kubectl exec -it pod-<username> -- /bin/bash
```

What does the network look like now?

What is the status of the files your created?

Finally, let’s delete explicitly the pod:

```
kubectl delete pod pod-<username>
```

## Let’s make it a deployment

You saw that when a pod was terminated, it was gone.

While above we did it by ourselves, the result would have been the same if a node died or was restarted.

In order to gain a higher availability, the use of Deployments is recommended. So, that’s what we will do next.

You can copy-and-paste the lines below, but please do replace “username” with your own id;\
As mentioned before, all the participants in this hands-on session share the same namespace, so you will get name collisions if you don’t.

###### dep1.yaml:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: dep-<username>
  labels:
    k8s-app: dep-<username>
spec:
  replicas: 1
  selector:
    matchLabels:
      k8s-app: dep-<username>
  template:
    metadata: 
      labels:
        k8s-app: dep-<username>
    spec:
      containers:
      - name: mypod
        image: centos:centos7
        resources:
           limits:
             memory: 1.5Gi
             cpu: 1
           requests:
             memory: 0.5Gi
             cpu: 0.1
        command: ["sh", "-c", "sleep infinity"]
```

Now let’s start the deployment:

```
kubectl create -f dep1.yaml
```

See if you can find it:

```
kubectl get deployments
```

Note: You may see the deployments from the other participants, too.

The Deployment is just a conceptual service, though.

See if you can find the associated pod:

```
kubectl get pods
```

Once you have found its name, let’s log into it

```
kubectl get pod -o wide dep-<username>-<hash>
kubectl exec -it dep-<username>-<hash> -- /bin/bash
```

You are now inside the (container in the) pod!

Create directories and files as before.

Try various commands as before.

Let’s now delete the pod!

```
kubectl delete pod dep-<username>-<hash>
```

Is it really gone?

```
kubectl get pods 
```

What happened to the deployment?

```
kubectl get deployments
```

Get into the new pod

```
kubectl get pod -o wide dep-<username>-<hash>
kubectl exec -it dep-<username>-<hash> -- /bin/bash
```

Was anything preserved?

Let’s now delete the deployment:

```
kubectl delete -f dep1.yaml
```

Verify everything is gone:

```
kubectl get deployments
kubectl get pods
```

## Horizontal scaling

Orchestration is often used to spread the load over multiple nodes.

In this exercise, we will launch multiple Web servers.

To make distinguishing the two servers easier, we will force the nodename into their homepages. Using stock images, we achieve this by using an init container.

You can copy-and-paste the lines below, but please do replace “username” with your own id;\
All the participants in this hands-on session share the same namespace, so you will get name collisions if you don’t.

Also in this one don't use the quick create method, you have to create a text file.

###### http2.yaml:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: http-<username>
  labels:
    k8s-app: http-<username>
spec:
  replicas: 2
  selector:
    matchLabels:
      k8s-app: http-<username>
  template:
    metadata: 
      labels:
        k8s-app: http-<username>
    spec:
      initContainers:
      - name: myinit
        image: busybox
        command: ["sh", "-c", "echo '<html><body><h1>I am ' `hostname` '</h1></body></html>' > /usr/local/apache2/htdocs/index.html"]
        volumeMounts:
        - name: dataroot
          mountPath: /usr/local/apache2/htdocs
      containers:
      - name: mypod
        image: httpd:alpine
        resources:
           limits:
             memory: 1.5Gi
             cpu: 1
           requests:
             memory: 0.5Gi
             cpu: 0.1
        volumeMounts:
        - name: dataroot
          mountPath: /usr/local/apache2/htdocs
      volumes:
      - name: dataroot
        emptyDir: {}
```

BTW: Feel free to change the number of replicas (within reason) and the text it is shown in home page of each server, if so desired.

Launch the deployment:

```
kubectl create -f http2.yaml
```

Also launch the pod1 from basic hand on excercise.

Check the pods you have, alongside the IPs they were assigned to:

```
kubectl get pods -o wide
```

Log into pod1

```
kubectl exec -it pod-<username> -- /bin/sh
```

Now try to pull the home pages from the two Web servers; use the IPs you obtained above:\
curl http://*IPofPod*

You should get a different answer from the two.

## Load balancing

Having to manually switch between the two Pods is obviously tedious. What we really want is to have a single logical address that will automatically load-balance between them.

You can copy-and-paste the lines below, but please do replace “username” with your own id;\
All the participants in this hands-on session share the same namespace, so you will get name collisions if you don’t.

###### svc2.yaml:

```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    k8s-app: svc-<username>
  name: svc-<username>
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    k8s-app: http-<username>
  type: ClusterIP
```

Let’s now start the service:

```
kubectl create -f svc2.yaml
```

Look up your service, and write down the IP it is reporting under:

```
kubectl get services
```

Log into pod1

```
kubectl exec -it pod-<username> -- /bin/sh
```

Now try to pull the home page from the service IP:\
curl http://*IPofService*

Try it a few times… Which Web server is serving you?

Note that you can also use the local DNS name for this (from pod1)

curl [http://svc-<username>.sc21.svc.cluster.local](http://svc-<username>.sc21.svc.cluster.local)

## Exposing public services

Sometimes you have the opposite problem; you want to export resources of a single node to the public internet.

The above Web services only serve traffic on the private IP network LAN. If you try curl from your laptop, you will never reach those Pods!

What we need is set up an Ingress instance for our service.

###### ingress.yaml:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: haproxy
  name: ingress-<username>
spec:
  rules:
  - host: <username>.nrp-nautilus.io
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: svc-<username>
            port:
              number: 80
  tls:
  - hosts:
    - <username>.nrp-nautilus.io
```

Launch the new ingress

```
kubectl create -f ingress.yaml
```

You should now be able to fetch the Web pages from your browser by opening <https://username.nrp-nautilus.io>. Note that SSL termination is already provided for you.

You can now delete the ingress:

```
kubectl delete -f ingress.yaml
```

## The end

**Please make sure you did not leave any running pods, deployments, ingresses or services behind.**
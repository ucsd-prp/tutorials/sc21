# SC21 Tutorial: Unified Cyber Infrastructure with Kubernetes

### Igor Sfiligoi, Dmitry Mishin

Kubernetes is the leading container orchestration solution, as it can work anywhere from commercial clouds to on-prem clusters and IoT devices. Federating such installations into a larger mesh of clusters makes those clusters a global supercomputer which can span multiple administrative and geographical regions.

One of Kubernetes advantages is its ability to effectively and securely utilize hardware resources for user jobs and system services, co-scheduling those as necessary on the same hardware. Service applications run with high privileges, with functionality and performance typical for bare metal deployments, while user applications are given only basic privileges.

In this tutorial, attendees will learn how to run their favorite software and run alongside other scientists on a nationwide Kubernetes cluster that is already serving hundreds of scientific groups through NFS-funded projects. The program includes a Kubernetes architectural overview and hands-on sessions operating on the PRP production Kubernetes clusters, including a federated ARM64 cluster.

https://sc21.supercomputing.org/presentation/?id=tut106&sess=sess183

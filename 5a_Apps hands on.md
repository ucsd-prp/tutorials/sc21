# SC21 Tutorial

Apps \
Hands on session

There is a package manager for Kubernetes called Helm. It simplifies deploying and configuring applications in your cluster.

You can choose one or several apps described below to deploy in this session.

## Installing helm

Refer to the documentation page to install helm in your OS: https://helm.sh/docs/intro/install/#through-package-managers

When installing the applications, please replace the `<username>` string with your own ID to avoid collisions.

You can list the deployed helm releases (including the ones deployed by others in the namespace) by running:

```
helm ls
```

## Postgres database

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm install <username>-postgres-release bitnami/postgresql
```

Once deployed, the helm chart will output the documentation on how to access the database. Try exploring the contents of the deployed database.

Also look at the StatefulSet, Service and PersistentVolumeClaim created by helm chart. The resulting installation is ready for production use and is using the persistent storage for the daatabase.

You can also delete the pod and see the new one created instead, using the same storage volume.

You can then run the client pod according to the information outputted by helm chart, and list the default databases:

`\l`

When you're done, remove the deployed application:

```
helm uninstall <username>-postgres-release
```

Note that helm doesn't delete the PersistentVolumeClaims created for you. Make sure you manually delete those.

## Redis in-memory key-value store

```
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm install <username>-redis-release bitnami/redis
```

Follow the helm chart instructions to connect to your redis instance and run some queries. You can also use the redis-cli installed on your local machine according to the instructions.

```
ping
```

```
set sc21key value123
```

```
get sc21key
```

Also look at the StatefulSet, Service and PersistentVolumeClaim created by helm chart. The resulting installation is ready for production use and is using the persistent storage for the database.

When you're done, remove the deployed application:

```
helm uninstall <username>-redis-release
```

Note that helm doesn't delete the PersistentVolumeClaims created for you. Make sure you manually delete those.

## Grafana timeseries visualization

Create the `grafana-values.yaml` file on your local machine to set up the grafana helm chart config, and pick the desired name to deploy it under by replacing the `<username>`:

```yaml
rbac:
  create: false
  pspEnabled: false
serviceAccount:
  create: false

ingress:
  enabled: true
  annotations:
    kubernetes.io/ingress.class: haproxy
  path: /
  hosts:
    - <username>.nrp-nautilus.io
  tls:
  - hosts:
    - <username>.nrp-nautilus.io

resources:
  limits:
    cpu: 100m
    memory: 128Mi
  requests:
    cpu: 100m
    memory: 128Mi
```

Then deploy the grafana helm chart using these values:

```
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
helm install <username>-grafana-release grafana/grafana -f grafana-values.yaml
```

Follow the instructions to get access to your instance. You should be able to see it in a browser at `<username>.nrp-nautilus.io`

Note that you can see the default configuration values that you can chage by running:

```
helm show values grafana/grafana
```

When you're done, remove the deployed application:

```
helm uninstall <username>-grafana-release
```

## Searching for other Helm repos

You can find more repos online. One of locations to check is https://artifacthub.io.

## Building your own image in GitLab

Login to https://gitlab.nrp-nautilus.io

```
Username: sc21
Password: nautilus
```

Create a new project:
* New project button on top
* New Project
* "Create blank project"
* Set the unique name and make it public. The name should be lowercase.
* Enable "Initialize repository with a README").
* Click "Create project"

Using Git is not the subject in this tutorial, so we'll be using the web UI to edit our new project.

Click "Web IDE" button

Create New file called .gitlab-ci.yml, and add this content:

```
image: gcr.io/kaniko-project/executor:debug
   
stages:
  - build-and-push
   
build-and-push-job:
  stage: build-and-push
  script:
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --cache=true --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:${CI_COMMIT_SHA:0:8} --destination $CI_REGISTRY_IMAGE:latest
```

Create another file called Dockerfile. Start with this (you can modify the file later):

```
FROM python:3

RUN pip install jupyter && \
  apt-get update && \
  apt-get install -y vim
```


Commit your changes using the button in the lower left corner. On the bottom you'll see your pipeline being executed.

You can go to CI/CD->Jobs to see your job running and either finished successfully, or with an error. You can see the build log and debug the process if needed.

Once the job is executed successfully, you should see your new image in `Packages & Registries`->`Container Registry`. There is also a button to copy the container image to clipboard.

Run the new image as a pod, exec into it and see if vim is installed together with jupyter:

```
root@test-pod:/# vim -h | head -n 1
VIM - Vi IMproved 8.1 (2018 May 18, compiled Jun 15 2019 16:41:15)
root@test-pod:/# jupyter
usage: jupyter [-h] [--version] [--config-dir] [--data-dir] [--runtime-dir] [--paths] [--json] [subcommand]
jupyter: error: one of the arguments --version subcommand --config-dir --data-dir --runtime-dir --paths is required
```

## End

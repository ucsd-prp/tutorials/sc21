# SC21 Tutorial

Federation \
Hands on session

## Deploying apps to an ARM cluster

In our main cluster there are no ARM-based nodes. What if you need one , for example to build a container for ARM?

If you try to request a pod like the one below, it will never be scheduled:

```
apiVersion: v1
kind: Pod
metadata:
  name: arm-pod-<username>
  namespace: sc21
spec:
  nodeSelector:
    kubernetes.io/arch: arm64
  containers:
  - name: vol-container
    image: busybox
    command: ["sleep", "infinity"]
```

You can check the list of nodes that match your requirements, and you will find no regular pods matching the ARM selector:

```
kubectl get nodes --selector='kubernetes.io/arch==arm64'
```

But you can see a something else... an entity labelled as a cluster!

Yes, we have a separate ARM-based cluster! And it is federated with the main k8s cluster you are using.

For security reasons, one has to be explicitly authorized to federate with it, but the namespace you're in is indeed federated to the same namespace int the ARM k8s cluster. Let's run a pod and ask the federation scheduler to decide where to place the pod. As you can see, it is just two additional lines in the metadata section:

```
apiVersion: v1
kind: Pod
metadata:
  annotations:
    multicluster.admiralty.io/elect: ""
  name: fed-pod-<username>
  namespace: sc21
spec:
  nodeSelector:
    kubernetes.io/arch: arm64
  containers:
  - name: vol-container
    image: busybox
    command: ["sleep", "infinity"]
```

Did it start? Where did it start?

Now log into the node and check that the architecture is indeed ARM:

```
kubectl exec fed-pod-isfiligoi -- /bin/sh -c "uname -a"
```

So, federation works great, right?

Try expanding some of the previous exercises to run on the ARM cluster, too.

## Limits of federated systems

Unfortunately, federation comes with some limits. In particular, there is no sharing of resources. If you wanted to access the persistent storage in our main cluster, you will be out of luck! E.g. the following will never be scheduled:

```
apiVersion: v1
kind: Pod
metadata:
  annotations:
    multicluster.admiralty.io/elect: ""
  name: fed-seaweed-<username>
  namespace: sc21
spec:
  nodeSelector:
    kubernetes.io/arch: arm64
  containers:
  - name: mypod
    image: centos:centos8
    command: ["sh", "-c", "sleep infinity"]
    resources:
      limits:
        memory: 600Mi
        cpu: 2
      requests:
        memory: 100Mi
        cpu: 100m
    volumeMounts:
    - mountPath: /examplevol
      name: examplevol
  volumes:
    - name: examplevol
      persistentVolumeClaim:
        claimName: sw-vol
```

A similar limitation applies to networking; i.e. two pods in different clusters cannot talk to each other (try to expand the Load balancing example to the federated cluster).

For completeness, you should be aware that there are additional services that allow for at least partial federation of networking and storage, but they are beyond the scope of this tutorial.

## End
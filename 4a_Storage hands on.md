# SC21 Tutorial

Storage in Kubernetes \
Hands on session

## Using storage

Different Kubernetes clusters will have different storage options available.\
Let’s explore the most basic one: emptyDir. It will allocate the local scratch volume, which will be gone once the pod is destroyed.

You can copy-and-paste the lines below, but please do replace “username” with your own id;\
As mentioned before, all the participants in this hands-on session share the same namespace, so you will get name collisions if you don’t.

###### ev.yaml:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: strg-<username>
  labels:
    k8s-app: strg-<username>
spec:
  replicas: 1
  selector:
    matchLabels:
      k8s-app: strg-<username>
  template:
    metadata: 
      labels:
        k8s-app: strg-<username>
    spec:
      containers:
      - name: mypod
        image: alpine
        resources:
           limits:
             memory: 600Mi
             cpu: 2
           requests:
             memory: 100Mi
             cpu: 100m
        command: ["sh", "-c", "apk add dumb-init && dumb-init -- sleep 100000"]
        volumeMounts:
        - name: mydata
          mountPath: /mnt/myscratch
      volumes:
      - name: mydata
        emptyDir: {}
```

Now let’s start the deployment:

`kubectl create -f strg1.yaml`

Now log into the created pod, create

`mkdir /mnt/myscratch/username`

then store some files in it.

Also put some files in some other (unrelated) directories.

Now kill the container: `kill 1`, wait for a new one to be created.

You can see that the `RESTARTS` counter increased - this means a container inside the pod was restarted.

```
kubectl get pod strg-<username>-<hash>
```

Then log back in.

What happened to files?

You can now delete the deployment.

## Using outer persistent storage

In our cluster we have multiple storage clisters connected, which allows using those for real data persistence.

To get storage, we need to create an abstraction called PersistentVolumeClaim. By doing that we "Claim" some storage space - "Persistent Volume". There will actually be PersistentVolume created, but it's a cluster-wide resource which you can not see.

Create the file (replace username as always):

###### pvc.yaml:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: vol-<username>
spec:
  storageClassName: rook-ceph-block
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

We're creating a 1GB volume and formatting it with XFS.

Look at it's status with `kubectl get pvc vol-<username` (replace username). The `STATUS` field should be equals to `Bound` - this indicates successful allocation.>

Now we can attach it to our pod. Create one with replacing `username`:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-<username>
spec:
  containers:
  - name: mypod
    image: centos:centos8
    command: ["sh", "-c", "sleep infinity"]
    resources:
      limits:
        memory: 600Mi
        cpu: 2
      requests:
        memory: 100Mi
        cpu: 100m
    volumeMounts:
    - mountPath: /examplevol
      name: examplevol
  volumes:
    - name: examplevol
      persistentVolumeClaim:
        claimName: vol-<username>
```

In volumes section we're attaching the requested persistent volume to the pod (by its name! Don't forget to change `username`), and in volumeMounts we're mounting the attached volume to the container in specified folder.

Now exec into your container and see the mounted rbd (Rados Block Device) volume:

`df`

The `ReadWriteOnce` accessMode we used means that only one node can mount this volume at a time. If your new pod is created on the same node, it may mount the volume, otherwise you'll get the Multi-Attach error.

Try creating a couple more pods with the same volume:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-2-<username>
spec:
  containers:
  - name: mypod
    image: centos:centos8
    command: ["sh", "-c", "sleep infinity"]
    resources:
      limits:
        memory: 600Mi
        cpu: 2
      requests:
        memory: 100Mi
        cpu: 100m
    volumeMounts:
    - mountPath: /examplevol
      name: examplevol
  volumes:
    - name: examplevol
      persistentVolumeClaim:
        claimName: vol-<username>
```

Which pods were created? Look in description for the reason:

```
kubectl describe pod pod-2-<username>
```

Clean up the pods and persistentvolumeclaim when you're done. Kubernetes will delete the corresponding persistentvolume and clean up the space taken by your volume.

If we wanted to run a distributed computation and have some data shared between the pods, we'd need to use the *shared filesystem*, which can be used by multiple nodes in parallel. You can see available shared filesystems on the portal page in the end of this tutorial.

There is already a volume created in the SeaweedFS filesystem called sw-vol (**please don't delete it!**). The volume has ReadWriteMany accessMode and can be used by all participants. Attach it to your pod and look at it's contents:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: pod-seaweed-<username>
spec:
  containers:
  - name: mypod
    image: centos:centos8
    command: ["sh", "-c", "sleep infinity"]
    resources:
      limits:
        memory: 600Mi
        cpu: 2
      requests:
        memory: 100Mi
        cpu: 100m
    volumeMounts:
    - mountPath: /examplevol
      name: examplevol
    - mountPath: /mysecretpath/sc21_s3cfg
      subPath: sc21_s3cfg
      name: s3-config
  volumes:
    - name: examplevol
      persistentVolumeClaim:
        claimName: sw-vol
    - name: s3-config
      secret:
        secretName: sw-s3-cfg
        items:
         - key: sc21_s3cfg
           path: sc21_s3cfg
```

SeaweedFS also provides access to the volumes through S3, WebDAV and other protocols. Let's look at it using S3.

In your last pod install the aws cli tool:


```
yum install -y python3-pip
pip3 install s3cmd
```

The credentials needed to access the SeaweedFS through S3 are mounted in /mysecretpath/sc21_s3cfg ;
you mounted them from a secrect we created for you as part of the YAML above.

But that's not where s3cmd expects them, so let's copy them in the right location:

```
cp /mysecretpath/sc21_s3cfg ~/.s3cfg
```

List the bucket:

```
s3cmd ls
```

Try uploading and downloading files:

```
touch my-super-file
s3cmd put my-super-file s3://pvc-8d8844a6-e9a3-4cb7-89e2-3774003f7b46/<username>/
```

Return to the pod and see your new file uploaded in `/examplevol/<username>`.

Delete your pod **BUT NOT THE SHARED VOLUME**

## Exploring storageClasses

Attaching persistent storage is usually done based on storage class. Different clusters will have different storageClasses, and you have to read the documentation on which one to use. The Nautilus storageClasses are described here: https://ucsd-prp.gitlab.io/userdocs/storage/ceph-posix/

You can create other persistentVolumes with the storageclasses which are not marked as restricted in the documentation.

## End

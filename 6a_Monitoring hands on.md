# SC21 Tutorial

Monitoring \
Hands on session

We use several monitoring systems in our cluster, which we will explore in this hands-on.

### Metrics server

See the current utilization for pods in sc21 namespace (and you can run your own pod):

`kubectl top pod`

Also check the current utilization of cluster nodes:

`kubectl top node`

### Perfsonar - the active bandwidth measurements

Look at https://perfsonar.nrp-nautilus.io/maddash-webui/ dashboard and see if there are any issues in connectivity between our cluster nodes.

The main institution to institution dashboard is https://perfsonar.nrp-nautilus.io/maddash-webui/index.cgi?grid=Nautilus%20Mesh%20-%20Latency%20general%20-%20Loss

### Prometheus + Thanos + Grafana

Find the pods you were running today in https://grafana.nrp-nautilus.io/d/6581e46e4e5c7ba40a07646395ef7b23/kubernetes-compute-resources-pod?var-namespace=sc21&orgId=1&refresh=10s and see what was the usage of CPU and memory.

Then find your GPU pods in https://grafana.nrp-nautilus.io/d/dRG9q0Ymz/k8s-compute-resources-namespace-gpus?orgId=1&var-namespace=sc21 and see if you used any significant portion of GPU. When running in our cluster, it's important to keep GPUs well utilized.

See whether you can spot any issues in the ceph cluster at https://grafana.nrp-nautilus.io/d/r6lloPJmz/ceph-cluster?orgId=1&refresh=1m.

### Elasticsearch

Login as guest to https://elastic-igrok.nautilus.optiputer.net/app/metrics/inventory and see how busy was the node you were running your pod on today

### Building the queries for new grafana dashboards

To make a new Grafana dashboard serving data from Prometheus, one has to write a PromQL query that will output the needed data. Let's make some examples.

Go to https://prometheus.nrp-nautilus.io/graph and enter in the expression field:

`kube_pod_info{namespace="sc21"}`

This expression will query the information about pods currently running in our namespace. We don't want to see all thousands of pods running in the cluster, so we're filtering the pods by the `namespace` label.

If you click the "Graph" button, you'll see the plot, but the value will always be 1. That's because this metric is only used for it's labels and doesn't have any useful measurements.

Let's find another metric with measurements that we could plot. Copy the pod name of one of the pods you see, and enter in the expression field:

`container_cpu_usage_seconds_total{namespace="sc21", pod="<pod_name>"}`

In the Table view you can see the current value, while the Graph gives you the history for the measurements. This metric provides the CPU usage of each container in the pod.

**Please don't query more than 1 hour of data, and only look at the sc21 namespace. Prometheus is hard to scale for multiple users, and it might run out of memory easily with this many participants.**

We were querying the single metrics (and filtering by labels), but the power of PromQL comes in joins, similar to what you can do in SQL. Let's make a joint query.

Let's see CPU statistics for all pods from SC21 namespace that were running on the same node. Choose the node your pod was running on and enter the query:

`kube_pod_info{namespace="sc21", node="the chosen node"} * on(namespace, pod) group_right() container_cpu_usage_seconds_total`

The join will allow multiple values on the right to be matched against unique values on the left.

Negative selector for the node will also work: `node!="the chosen node"`

There are other useful functions in PromQL. For example if your metric is constantly growing, like the number of reads from disk:

`node_disk_reads_completed_total{instance="k8s-epyc-01.sdsc.optiputer.net",device=~"nvme.*"}`

you can look at the rate (I/O operations per second, IOPS):

`irate(node_disk_reads_completed_total{instance="k8s-epyc-01.sdsc.optiputer.net",device=~"nvme.*"}[1m])`

Many metrics in Prometheus are computed once the new data arrives, for each time point, to speed up the queries in case the computation involves multiple other metrics. You can browse the existing computed metrics in https://prometheus.nrp-nautilus.io/rules.

## End
